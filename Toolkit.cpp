#include "Toolkit.h"

double Toolkit::Show_AO()
{
	std::cout<<V.rows()<<"   "<<N.rows()<<std::endl;


	double AO_I = 0.;
	for (int i = 0; i < AO.rows(); i++)
	{
		AO_I += AO(i)/AO.rows();
	}

	printf("AO number is : %.4f\n", AO_I);
	printf("Last AO number is : %.4f\n",AO(AO.rows()-1));
	return AO_I;
}

double Toolkit::AO_Integrate(std::vector<Point> list)
{
#ifndef _DEBUG 
	if(list.size()==0) { return -1 ;}
	double result = 0.;
	for(int i = 0 ; i < list.size() ; i++)
	{
		Primitive_id p_id = tree.closest_point_and_primitive(list[i]).second;
		vertex_handle closest = p_id->halfedge()->vertex();
		std::map<vertex_handle,double>::iterator iter = AO_MAP.find(closest);
		if(iter!=AO_MAP.end())
		{
			result += (iter->second / list.size());
		}
	}
	return result;
#else
	return 1.0;
#endif
}

Toolkit::Point Toolkit::refl(const Point input,const Plane & pl)
{
	const double x = input.x();
	const double y = input.y();
	const double z = input.z();
	const double t = (pl.a()*x+pl.b()*y+pl.c()*z+pl.d())/(pl.a()*pl.a()+pl.b()*pl.b()+pl.c()*pl.c());
	return Point(x-2*pl.a()*t,y-2*pl.b()*t,z-2*pl.c()*t);
}

int Toolkit::Find_Reflect()
{
	typedef CGAL::AABB_face_graph_triangle_primitive<Polyhedron>         Facet_Primitive;
	typedef CGAL::AABB_traits<Kernel, Facet_Primitive>                  Facet_Traits;
	typedef CGAL::AABB_tree<Facet_Traits>                               Facet_tree;
	printf("\n");
	std::ifstream ref = std::ifstream("symmetric.dat");
	Facet_tree *face_tree = new Facet_tree;
	Point center = Point(0,0,0);
	Polyhedron cpy;
	char* buf = new char[100];
	std::vector<Point> vt_p;
	// Calculate centroid vertex
	int nb_vertex = mesh.size_of_vertices();
	for(vertex_iterator it = mesh.vertices_begin(),end = mesh.vertices_end();it!=end;it++)
	{
		center = Point((*it).point().x()/nb_vertex +center.x(),(*it).point().y()/nb_vertex +center.y()
			,(*it).point().z()/nb_vertex +center.z());
		vt_p.push_back((*it).point());
	}
	double res[3] = {0.};
	double max_v = 9999;
	while(ref.getline(buf,100))
	{
		double x,y,z;
		sscanf(buf,"%lf%lf%lf",&x,&y,&z);
		printf(".");
		// Construct plane
		Plane pl = Plane(center,Vector(x,y,z));
		double min_v = -9999;
		cpy = mesh;
		for(vertex_iterator it = cpy.vertices_begin(),end = cpy.vertices_end();it!=end;it++)
		{
			(*it).point() = refl((*it).point(),pl);
		}
		face_tree->clear();
		face_tree->rebuild(faces(cpy).first, faces(cpy).second, cpy);
		face_tree->accelerate_distance_queries();
		for(std::vector<Point>::iterator it = vt_p.begin(),end = vt_p.end();it!=end;it++)
		{
			Point closest = face_tree->closest_point((*it));
			const double dis = CGAL::to_double(((*it).x()-closest.x())*((*it).x()-closest.x()) + 
				((*it).y()-closest.y())*((*it).y()-closest.y()) + 
				((*it).z()-closest.z())*((*it).z()-closest.z())) ;
			if(dis > min_v)
				min_v = dis;
		}
		if(min_v < max_v)
		{
			max_v = min_v;
			res[0] = x ;
			res[1] = y ;
			res[2] = z ;
		}
	}
	if(max_v > GetDiagonal()) 
	{
		return 0;		// Larger than Diagonal , return 0 ; 
	}
	SetSymmetric(true);			// Has Symmetric Plane
	Symm = Plane(center,Vector(res[0],res[1],res[2]));
	printf("\nSymmetric plane parameters :  %.2f  %.2f  %.2f \n",res[0],res[1],res[2]);
	printf("Symmetric value : %.4f\n",max_v);
	ref.close();
	return 1;
}

double Toolkit::Volume_OBB(Polyhedron & poly)
{
	typedef CGAL::Delaunay_triangulation_3<K>                       Delaunay;
	typedef Delaunay::Vertex_handle                                 Vertex_handle;
	CGAL::Polyhedron_3<Kernel> CH_3;
	std::list<Point3> container;
	Delaunay T;
	for(Polyhedron::Point_iterator it = poly.points_begin();
		it != poly.points_end(); it++)
	{
		container.push_back(*it);
	}
	T.insert(container.begin(), container.end());
	CGAL::convex_hull_3_to_polyhedron_3(T,CH_3);
	FT V_Min = 10000000.0;
	Polygon_2 convex_hull,min_rectangle;
	// Calculate OBB using Rotating Calipers
	for(Polyhedron::Facet_iterator it = CH_3.facets_begin(),nd = CH_3.facets_end();
		it != nd; it++)
	{
		// Easy algorithm for normal
		const Polyhedron::Halfedge_handle h = it->halfedge();
		const Plane p = Plane( h->vertex()->point(),
			h->next()->vertex()->point(),
			h->opposite()->vertex()->point());
		Vector normal = p.orthogonal_vector();
		normal = normal / std::sqrt(normal.squared_length());
		const Point e = h->vertex()->point();
		/*
		Point e = it->halfedge()->vertex()->point();
		HF_circulator he = it->facet_begin();
		HF_circulator end = he;
		CGAL_For_all(he,end)
		{
			const Point& prev = he->prev()->vertex()->point();
			const Point& curr = he->vertex()->point();
			const Point& next = he->next()->vertex()->point();
			const Vector n = CGAL::cross_product(next-curr,prev-curr);
			normal = normal + n;
		}
		normal = normal / std::sqrt(normal.squared_length());
		
		// 		std::cout << " Normal vector is "<<normal<<"  Length is "<<normal.squared_length()<<std::endl;
		// 		system("pause");
		Plane p = Plane(e,normal);
		*/
		std::vector<Point_2> points;
		Vector basis[2];
		// 根据平面求平面上的两个正交向量 basis[0] 和 basis[1]
		// base[2]  为平面的垂线
		const FT base_0 = p.base1().squared_length();
		const FT base_1 = p.base2().squared_length(); 
		// 基向量
		basis[0] = p.base1()/sqrt(base_0);
		basis[1] = p.base2()/sqrt(base_1);
		double Dis_M = 0.0 ;
		for(Polyhedron::Vertex_iterator vt = CH_3.vertices_begin(),vd = CH_3.vertices_end();
			vt != vd ; vt++)
		{
			const Vector terminal = Vector(vt->point().x()-e.x(),vt->point().y()-e.y(),vt->point().z()-e.z());
			Point_2 pt = Point_2(basis[0]*terminal,basis[1]*terminal);
			points.push_back(pt);
			double Dis =fabs(normal * terminal);
			if(Dis > Dis_M)
			{
				Dis_M = Dis ;
			}
		}
		convex_hull.clear();
		CGAL::convex_hull_2(points.begin(), points.end(), std::back_inserter(convex_hull));
		min_rectangle.clear();
		CGAL::min_rectangle_2(convex_hull.vertices_begin(), convex_hull.vertices_end(), std::back_inserter(min_rectangle));
		double V =fabs(min_rectangle.area()) * Dis_M;
		if(V < V_Min)
		{
			V_Min = V;
		}
	}
	//printf("%.4f\n",V_Min);
	return V_Min;
}
 
unsigned int Toolkit::V_OBB(Polyhedron & ply , const double printv)
{
	if(ply.size_of_vertices() < 3)
		return 0.0 ;
	// Construct memory
	unsigned int num =  ply.size_of_vertices();
	if(num_points < num )
	{
		if(points != NULL)
		{
			free(points); free(pnt_arr);
			num_points =  static_cast<unsigned int>(1.3 * num);
			points = (gdiam_point)malloc( sizeof( gdiam_point_t ) * num_points );
			pnt_arr = (gdiam_point *)malloc( sizeof( gdiam_point ) * num_points );
		}
		else
		{
			num_points =  static_cast<unsigned int>(1.3 * num);
			points = (gdiam_point)malloc( sizeof( gdiam_point_t ) * num_points );
			pnt_arr = (gdiam_point *)malloc( sizeof( gdiam_point ) * num_points );
		}
	}
	assert( points != NULL );
	int ind = 0 ;
	for(Polyhedron::Point_const_iterator it = ply.points_begin();
		it != ply.points_end(); it++ , ind ++)
	{
		points[ ind * 3 + 0 ] = it->x();
		points[ ind * 3 + 1 ] = it->y();
		points[ ind * 3 + 2 ] = it->z();
	}
	gdiam_bbox   bb;

	for  ( int  ind = 0; ind < num; ind++ ) 
	{
		pnt_arr[ ind ] = (gdiam_point)&(points[ ind * 3 ]);
	}

	bb = gdiam_approx_mvbb(pnt_arr,num,1e-6);
	//bb = gdiam_approx_mvbb_grid_sample( pnt_arr, num, 5, 400 );
	gdiam_real length[3];
	bb.get_length(length);
	unsigned int obb = 1 ;
	for(int i = 0 ; i < 3 ; i ++)
	{
		obb = obb * std::ceil(length[i]/printv);
	}
	
	//printf("Len1 = %.3f  Len2 = %.3f  Len3 = %.3f\n",length[0],length[1],length[2]);

	return obb;
	//return bb.volume() ;

	//return 0 ;
	/*
	re.clear();
	std::set<CDT::Vertex_handle> cvt;
	TRI tri;
	int count=0;
	for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin();
		fit!=cdt.finite_faces_end();++fit)
	{
		if ( fit->is_in_domain())  
		{ 
			for(int i = 0 ; i < 3 ; i++)
			{
				cvt.insert(fit->vertex(i));
			}
		}
	}
	printf("SET NUMBER = %d\n",cvt.size());


	struct TRI_SEG{
		CDT::Vertex_handle source;
		CDT::Vertex_handle target;
		bool operator <(TRI_SEG const& _A) const
		{
			if(source < _A.source) { return true; }
			else if(source == _A.source)
			{
				if(target < _A.target) { return true; }
				else { return false; }
			}
			else { return false;}
		}
	} tri_seg;

	std::set<TRI_SEG> list;
	for(std::set<CDT::Vertex_handle>::const_iterator sit = cvt.begin();
		sit != cvt.end(); sit ++)
	{
		//std::cout <<"I'm "<< (*sit)->point() <<std::endl;
		CDT::Face_circulator fc = (*sit)->incident_faces();
		CDT::Face_circulator fd = fc;
		CGAL_For_all(fc,fd)
		{
			if(fc->is_in_domain())
			{
				for(int m = 0 ; m < 3 ; m++)
				{
					if( fc->vertex(m) < (*sit))
					{
						tri_seg.source = fc->vertex(m);
						tri_seg.target = (*sit);
						list.insert(tri_seg);
					}
					else if( fc->vertex(m) > (*sit) )
					{
						tri_seg.source = (*sit);
						tri_seg.target = fc->vertex(m);
						list.insert(tri_seg);
					}
				}
			}
		}
	}
	printf("size of list : %d\n",list.size());
	for(std::set<TRI_SEG>::iterator it = list.begin();
		it != list.end(); it++)
	{
		tri.source = pl.to_3d(it->source->point());
		tri.target = pl.to_3d(it->target->point());
		//std::cout << it->source->point() <<" -> " <<it->target->point()<<std::endl;
		//std::cout << tri.source <<" -> " <<tri.target<<std::endl;
		re.push_back(tri);
	}
	printf("size of re : %d\n",re.size());
	*/

}

double Toolkit::GetDiagonal()
{
	return diagonal ; 
}

void Toolkit::CalDiagonal()
{
	if(mesh.size_of_vertices() < 3)
		return;
	gdiam_real  * points;
	// Construct memory
	const unsigned int num = mesh.size_of_vertices();
	points = (gdiam_point)malloc( sizeof( gdiam_point_t ) * num );
	assert( points != NULL );
	int ind = 0 ;
	for(Polyhedron::Vertex_const_iterator it = mesh.vertices_begin();
		it != mesh.vertices_end(); it ++ , ind ++)
	{
		points[ ind * 3 + 0 ] = CGAL::to_double(it->point().x());
		points[ ind * 3 + 1 ] = CGAL::to_double(it->point().y());
		points[ ind * 3 + 2 ] = CGAL::to_double(it->point().z());
	}

	gdiam_point  * pnt_arr;
	gdiam_bbox   bb;
	pnt_arr = gdiam_convert( (gdiam_real *)points, num );

	bb = gdiam_approx_mvbb(pnt_arr,num,0.5);
	diagonal = bb.diagonal();
	free(points);
}

void Toolkit::Open_File(std::string filename)
{
	if(edgefile == NULL )
	{
		edgefile.open(filename,std::ios::out|std::ios::app);
	}
	else
	{
		edgefile.close();
		edgefile.open(filename,std::ios::out|std::ios::app);
	}
}

void Toolkit::Close_File()
{
	if(edgefile != NULL)
	{
		edgefile.close();
	}
}

void Toolkit::Clear_File(std::string filename)
{
	if(edgefile != NULL)
	{
		edgefile.close();
		edgefile.open(filename);
		edgefile.close();
		Open_File(filename);
	}
	else
	{
		edgefile.open(filename);
		edgefile.close();
		Open_File(filename);
	}
}

void Toolkit::Dimension(Point s,Point t,Plane pl,Point pop)
{
	Vector basis[2];
	const FT base_0 = pl.base1().squared_length();
	const FT base_1 = pl.base2().squared_length(); 
	basis[0] = pl.base1()/sqrt(base_0);
	basis[1] = pl.base2()/sqrt(base_1);
	char buf[1000];
	const Vector ter1 = Vector(s.x()-pop.x(),s.y()-pop.y(),s.z()-pop.z());
	const Vector ter2 = Vector(t.x()-pop.x(),t.y()-pop.y(),t.z()-pop.z());
	sprintf(buf,"%.4f %.4f %.4f %.4f\n",ter1*basis[0],ter1*basis[1],ter2*basis[0],ter2*basis[1]);
	edgefile<<buf;
}

void Toolkit::Dimension(Point_2 s,Point_2 t)
{
	char buf[1000];
	sprintf(buf,"%.18f %.18f %.18f %.18f\n",s.x(),s.y(),t.x(),t.y());
	edgefile<<buf;
}

void Toolkit::Init_Num(int n)
{
	char buf[100];
	sprintf(buf,"%d\n",n);
	edgefile<<buf;
}


Toolkit::Vector Toolkit::compute_facet_normal(const Facet& f)
{
	typedef  Facet::Halfedge_around_facet_const_circulator HF_circulator;
	Vector normal = CGAL::NULL_VECTOR;
	HF_circulator he = f.facet_begin();
	HF_circulator end = he;
	CGAL_For_all(he,end)
	{
		const Point& prev = he->prev()->vertex()->point();
		const Point& curr = he->vertex()->point();
		const Point& next = he->next()->vertex()->point();
		Vector n = CGAL::cross_product(next-curr,prev-curr);
		normal = normal + (n / std::sqrt(n*n));
	}
	return normal / std::sqrt(normal * normal);
}

Toolkit::Vector Toolkit::compute_vertex_normal(Polyhedron::Vertex_iterator v)
{
	typedef  Vertex::Halfedge_around_vertex_const_circulator HV_circulator;
	typedef  Vertex::Facet Facet;
	Vector normal = CGAL::NULL_VECTOR;
	HV_circulator he = v->vertex_begin();
	HV_circulator end = he;
	CGAL_For_all(he,end)
	{
		if(!he->is_border())
		{
			Vector n = compute_facet_normal(*he->facet());
			normal = normal + (n / std::sqrt(n*n));
		}
	}
	return normal / std::sqrt(normal * normal);
}

// 
// #define Tollerance 0.05
// bool Toolkit::Search_SFra(Plane pl)
// {
// 	const Vector normal_pl = pl.orthogonal_vector()/ std::sqrt(pl.orthogonal_vector().squared_length());
// 	for(std::vector<Point>::iterator it = S.begin();it!=S.end();it++)
// 	{
// 		const Vector origin = Vector(it->x()-pl.point().x(),it->y()-pl.point().y(),it->z()-pl.point().z());
// 		if(origin * normal_pl < Tollerance)
// 		{
// 			return true;
// 		}
// 	}
// 	return false;
// }


bool Toolkit::Initial_Printer(Polyhedron& poly)
{
	typedef Polyhedron::Facet_iterator kl;
	// Calculate AABB Box

	// Modify vertex coordinates

	// Repair 
	return false;
}

void Toolkit::Load_Edgefile(std::string filename)
{
	cdt.clear();		// Rebuild constrained triangulation object
	std::ifstream ifs(filename);
	bool first=true;
	int n;
	ifs >> n;

	K::Point_2 p,q, qold;

	CDT::Vertex_handle vp, vq, vqold;
	while(ifs >> p) 
	{
		ifs >> q;
		if(p == q)
		{
			continue;
		}
		if((!first) && (p == qold))
		{
			vp = vqold;
		} 
		else 
		{
			vp = cdt.insert(p);
		}
		vq = cdt.insert(q, vp->face());
		if(vp != vq) 
		{
			cdt.insert_constraint(vp,vq);
		}
		qold = q;
		vqold = vq;
		first = false;
	}
	initializeID(cdt);
	discoverComponents(cdt);
}

void Toolkit::discoverComponents(const CDT & ct)
{
	if (ct.dimension()!=2) return;
	int index = 0;
	std::list<CDT::Edge> border;
	discoverComponent(ct, ct.infinite_face(), index++, border);
	while(! border.empty())
	{
		CDT::Edge e = border.front();
		border.pop_front();
		Face_handle n = e.first->neighbor(e.second);
		if(n->counter() == -1)
		{
			discoverComponent(ct, n, e.first->counter()+1, border);
		}
	}
}

void Toolkit::discoverComponent(const CDT & ct, Face_handle start,int index, std::list<CDT::Edge>& border )

{
	if(start->counter() != -1)
	{
		return;
	}
	std::list<Face_handle> queue;
	queue.push_back(start);

	while(! queue.empty())
	{
		Face_handle fh = queue.front();
		queue.pop_front();
		if(fh->counter() == -1)
		{
			fh->counter() = index;
			fh->set_in_domain(index%2 == 1);
			for(int i = 0; i < 3; i++)
			{
				CDT::Edge e(fh,i);
				Face_handle n = fh->neighbor(i);
				if(n->counter() == -1)
				{
					if(ct.is_constrained(e))
					{
						border.push_back(e);
					} 
					else 
					{
						queue.push_back(n);
					}
				}
			}
		}
	}
}

void Toolkit::initializeID(const CDT& ct)
{
	for(All_faces_iterator it = ct.all_faces_begin();
		it != ct.all_faces_end(); 
		it++)
	{
		it->set_counter(-1);
	}
}

bool Toolkit::FillHole(Polyhedron & mesh)
{
	// Normalize border
	mesh.normalize_border();
	if(!mesh.normalized_border_is_valid()) { return false ;}
	if(mesh.size_of_border_halfedges())
	{
		while(mesh.size_of_border_edges() && mesh.normalized_border_is_valid())
		{
			for(halfedge_iterator it = mesh.border_halfedges_begin();
				it != mesh.halfedges_end(); it ++)
			{
				++ it;
				halfedge_handle start , end;
				start = end = it ; 
				do 
				{
					start = start->next();
				} while (start != end);
				mesh.fill_hole(it);
				mesh.normalize_border();
				break;
			}
		}
		CGAL::triangulate_polyhedron<Polyhedron> (mesh);
		return true;
	}
	else
	{
		return true;
	}
}

void Toolkit::CopyPolyhedron(Polyhedron start , Polyhedron & end)
{
	end.reserve(start.size_of_vertices(), start.size_of_halfedges(), start.size_of_facets());
	std::copy(start.vertices_begin(), start.vertices_end(), end.vertices_begin());
	std::copy(start.edges_begin(),start.edges_end(),end.edges_begin());
	std::copy(start.halfedges_begin(), start.halfedges_end(), end.halfedges_begin());
	std::copy(start.facets_begin(), start.facets_end(), end.facets_begin());
	
	return ;
}

double Toolkit::ConnectorsFeasible( )
{
	typedef CDT::All_faces_iterator All_faces_iterator;
	std::stack<CDT::Face_handle> FacesStack;
	std::set<Point_2> PolygonSet;
	std::vector<double> AreaArray;
	All_faces_iterator t , s;
	while(!FacesStack.empty())  { FacesStack.pop(); }
	int  noc = 0 ;
	for(All_faces_iterator it = cdt.all_faces_begin();
		it != cdt.all_faces_end() ; it ++  )
	{
		it->set_mask(0);
	}
	for(All_faces_iterator it = cdt.all_faces_begin();
		it != cdt.all_faces_end() ; it ++  )
	{
		if(it->is_in_domain() && it->get_mask() == 0)
		{
			double Area = 0 ;
			noc ++ ;
			it->set_mask(noc) ;
			FacesStack.push(it);
			while(!FacesStack.empty())
			{
				t = FacesStack.top();
				Area =	Area + t->vertex(0)->point().x()*t->vertex(1)->point().y() - t->vertex(0)->point().x()*t->vertex(2)->point().y() -t->vertex(1)->point().x()*t->vertex(0)->point().y()
					+ t->vertex(2)->point().x()*t->vertex(0)->point().y() + t->vertex(1)->point().x()*t->vertex(2)->point().y() -t->vertex(2)->point().x()*t->vertex(1)->point().y();
				FacesStack.pop();
				for(int j = 0 ; j < 3 ; j++)
				{	
					if(t->neighbor(j)->is_in_domain() &&  (t->neighbor(j)->get_mask()==0) )
					{
						t->neighbor(j) ->set_mask(noc);
						FacesStack.push(t->neighbor(j));
					}
				}
			}
			AreaArray.push_back(Area/2);
		}
	}
	double ReturnValue = 0 , MaxValue = -100 ;
	Polygon_2 convex_hull;
	if(noc == 0 )
	{
		return 0.0;
	}
	else
	{
		for(int i = 1 ; i <= noc ; i ++)
		{
			if( ! PolygonSet.empty() ) { PolygonSet.clear() ; }
			if(! convex_hull.is_empty()) { convex_hull.clear(); }
			for(All_faces_iterator it = cdt.all_faces_begin();
				it != cdt.all_faces_end() ; it ++  )
			{
				if( i == it->get_mask())
				{
					for(int j = 0 ; j < 3 ; j ++)
					{
						PolygonSet.insert(it->vertex(j)->point());
					}
				}
			}
			CGAL::ch_melkman(PolygonSet.begin(),PolygonSet.end(),back_inserter(convex_hull));
			ReturnValue = AreaArray[i-1]/convex_hull.area();
			if( ReturnValue > MaxValue )
			{
				MaxValue = ReturnValue ;
			}
		}
		return MaxValue ;
	}
}