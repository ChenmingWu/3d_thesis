#ifndef SCENE_H
#define SCENE_H
#undef min
#undef max
#include "Toolkit.h"
#include <vector>
#include <QtOpenGL/qgl.h>
#include <iostream>
#include <cmath>
#include <stack>
#include <CGAL/AABB_intersections.h> 
#include "types.h"
#include "Color_ramp.h"
#include "axisb.h"
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/AABB_halfedge_graph_segment_primitive.h>
#include <CGAL/AABB_face_graph_triangle_primitive.h>
#include <CGAL/bounding_box.h>
#include <set>

#include <QtCore/qglobal.h>
#include <QGLViewer/manipulatedFrame.h>
#include <QGLViewer/qglviewer.h>

class Scene : public QObject
{
	Q_OBJECT
public:
	Scene();
	virtual ~Scene();
public:
	// types
	typedef CGAL::Bbox_3 Bbox;

private:
	typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
	typedef CGAL::AABB_face_graph_triangle_primitive<Polyhedron>         Facet_Primitive;
	typedef CGAL::AABB_traits<Kernel, Facet_Primitive>                  Facet_Traits;
	typedef CGAL::AABB_tree<Facet_Traits>                               Facet_tree;
	typedef Polyhedron::Facet									 Facet;
	typedef Polyhedron::Facet_handle							 Facet_handle;
	typedef Polyhedron::Facet_iterator							 Facet_iterator;
	typedef CGAL::AABB_halfedge_graph_segment_primitive<Polyhedron>     Edge_Primitive;
	typedef CGAL::AABB_traits<Kernel, Edge_Primitive>                   Edge_Traits;
	typedef CGAL::AABB_tree<Edge_Traits>                                Edge_tree;
	typedef std::vector<Facet_tree::Object_and_primitive_id>		Intersections;
	typedef qglviewer::ManipulatedFrame ManipulatedFrame;

	enum Cut_planes_types {
		NONE, UNSIGNED_FACETS, SIGNED_FACETS, UNSIGNED_EDGES, CUT_SEGMENTS
	};

public:
	void draw(); 
	void update_bbox();
	Bbox bbox() { return m_bbox; }
	ManipulatedFrame* manipulatedFrame() const { return m_frame; }

private:
	Toolkit *tkt;
	// member data
	Bbox m_bbox;
	axisb *toolkit;
	Polyhedron *m_pPolyhedron;
	Polyhedron *pos_polyhedron;
	Polyhedron *neg_polyhedron;
	std::vector<Facet_tree::Object_and_primitive_id> intersections;
	std::list<Point> m_points;
	std::list<Segment> m_segments;
	std::vector<Segment> m_cut_segments;
	std::vector<Polyhedron::Facet_handle> m_facet_handles;
	std::vector<Polyhedron::Halfedge_handle> m_hs_handles,m_ht_handles,m_hd_handles;
	std::vector<Point> m_point;
	std::vector<Segment> cut_planes;
	std::vector<std::vector<Point> > seam_points;
	std::vector<std::vector<Point> > new_seams;
	double diameter;
	double theta;

	bool GLOBAL_F ;
	struct MyUnion{
		Point source;
		Point target;
		Polyhedron::Facet_handle facet;
		Polyhedron::Halfedge_handle handle;
		unsigned int flag;
	};

	struct BSP
	{

		Polyhedron poly;		// Without holes
		Polyhedron *poly_hole;	// With holes
		double score;			// BSP's score
		double OBB;				// BSP's Oriented-bounding box volume
		double V;				// BSP's volume
		Plane cut_plane;		// Cut plane
		//Override LT operator
		bool operator < (const BSP& t) const 
		{
			if( std::fabs(t.OBB - OBB) < 1e-6 )
			{
				return t.V < V;
			}
			else
			{
				return t.OBB < OBB;
			}
		}
	};


	unsigned tree_cursor ;
	std::set<Point> source_set;
	std::set<Point> target_set;
	std::vector<MyUnion> myunion;
	//Bsp tree here
	double PRINTER_V; 
	std::vector<std::multiset<BSP> > BSP_Tree;
	//std::vector<Facet_tree> BSP_AABB;
	bool intersected;
	BSP *tree;
	struct cp{
		Plane plane;
		bool  live;
	};
	std::vector<cp> candidate_planes;
	const std::stack<Plane> init_planes;
	// distance functions (simple 2D arrays)
	Color_ramp m_red_ramp;
	Color_ramp m_blue_ramp;
	Color_ramp m_thermal_ramp;
	FT m_max_distance_function;
	typedef std::pair<Point,FT> Point_distance;
	Point_distance m_distance_function[100][100];

	// frame
	ManipulatedFrame* m_frame;
	bool m_view_plane;
	int m_grid_size;
	bool m_fast_distance;

	// An aabb_tree indexing polyhedron facets/segments
	Facet_tree m_facet_tree;
	Edge_tree m_edge_tree;

	Cut_planes_types m_cut_plane;
private:
	// utility functions
	Vector random_vector();
	Ray random_ray(const Bbox& bbox);
	Line random_line(const Bbox& bbox);
	Point random_point(const Bbox& bbox);
	Plane random_plane(const Bbox& bbox);
	Segment random_segment(const Bbox& bbox);
	FT random_in(const double a,const double b);
	Plane frame_plane() const;
	Aff_transformation frame_transformation() const;
	FT bbox_diag() const;
	void build_facet_tree();
	void build_edge_tree();
	void clear_internal_data();
	void update_grid_size();
	template <typename Tree>
	void compute_distance_function(const Tree& tree);
	void load_planes();
	template <typename Tree>
	void sign_distance_function(const Tree& tree);
	double bsp_insert(Polyhedron poly ,std::multiset<BSP> & bsp_tree);
public:
	// file menu
	int open(QString filename);
	double cut_with_plane(BSP bsps , Plane plane , std::multiset<BSP> & bsp_tree , int b , int rank);

	// edit menu
	void clear_points() { m_points.clear(); }
	void clear_segments() { m_segments.clear(); }
	void clear_cutting_plane();

	// fast distance setter
	void set_fast_distance(bool b) { m_fast_distance = b; update_grid_size(); }

	// algorithms
	void generate_edge_points(const unsigned int nb_points);
	void generate_inside_points(const unsigned int nb_points);
	void generate_boundary_points(const unsigned int nb_points);
	void generate_boundary_segments(const unsigned int nb_slices);
	void generate_points_in(const unsigned int nb_points,
		const double min, const double max);

	// algorithms/refine
	void refine_loop();
	void refine_bisection(const FT max_sqlen);

	// distance functions 
	void signed_distance_function();
	void unsigned_distance_function();
	void unsigned_distance_function_to_edges();
	void cut_segment_plane();
	void repair_non_triangle(Polyhedron::Halfedge_handle h,Polyhedron *m_p);
	void cut_polyhedron();
	// toggle view options
	void toggle_view_points();
	void toggle_view_segments();
	void toggle_view_poyhedron();
	void toggle_view_plane();


	void DfsComponent(Facet_iterator i, int nComponent);
	// view options
	bool m_view_points;
	bool m_view_segments;
	bool m_view_polyhedron;

	// benchmarks 
	enum {DO_INTERSECT,
		ANY_INTERSECTION,
		NB_INTERSECTIONS,
		ALL_INTERSECTIONS,
		ANY_INTERSECTED_PRIMITIVE,
		ALL_INTERSECTED_PRIMITIVES};
	void bench_memory();
	void bench_construction();
	void bench_distances_vs_nbt();
	void bench_intersections_vs_nbt();
	void benchmark_intersections(const double duration);
	std::size_t nb_digits(const std::size_t value);

	template <class Query>
	void bench_intersection(Facet_tree& tree,const int function,const double duration,
		const char *query_name, const std::vector<Query>& queries, const int nb_queries);
	void bench_intersections(Facet_tree& tree, const double duration, const int function,
		const char *function_name, const std::vector<Ray>& rays,
		const std::vector<Line>& lines, const std::vector<Plane>& planes,
		const std::vector<Segment>& segments, const int nb_queries);

	// distance benchmarks
	enum {SQ_DISTANCE,
		CLOSEST_POINT,
		CLOSEST_POINT_AND_PRIMITIVE_ID};
	void benchmark_distances(const double duration);
	void bench_closest_point(Facet_tree& tree,const double duration);
	void bench_squared_distance(Facet_tree& tree,const double duration);
	void bench_closest_point_and_primitive(Facet_tree& tree,const double duration);
	void bench_distance(Facet_tree& tree,const int function,const double duration);

	// drawing
	void draw_points();
	void draw_segments();
	void draw_polyhedron();
	void draw_distance_function(const Color_ramp& ramp_pos,
		const Color_ramp& ramp_neg) const;
	void draw_cut_segment_plane() const;

	// cutting plane activation/deactivation
	void activate_cutting_plane();
	void deactivate_cutting_plane();
public:
	double get_aabb(Polyhedron poly_3);
	int eulerUpdate(Polyhedron& P1 );
	double get_volume(Polyhedron poly_3);
	void save_cut_off(std::multiset<BSP> bsp);
	public slots:
	// cutting plane
	void cutting_plane();
	bool similarity(Point a,Point & b);
	bool positive(Plane & p, Point & a);
	bool negative(Plane & p, Point & a);
	bool my_parallel(Point & a,Point & b,Point & c);
	double OBB(Polyhedron & poly);
}; // end class Scene

#endif // SCENE_H
