#include <readOFF.h>
#include <per_vertex_normals.h>
#ifndef _DEBUG 
#include <embree/ambient_occlusion.h>
#endif
// Approximation minimum volume bounding box
//#include "gdiam.hpp"

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/AABB_face_graph_triangle_primitive.h>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_mesher_2.h>
#include <CGAL/Delaunay_mesh_face_base_2.h>
#include <CGAL/Delaunay_mesh_size_criteria_2.h>
#include <CGAL/Lipschitz_sizing_field_2.h>
#include <CGAL/Lipschitz_sizing_field_criteria_2.h>
#include <CGAL/Triangulation_conformer_2.h>
#include <CGAL/triangulate_polyhedron.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <CGAL/Min_ellipse_2.h>
#include <CGAL/Min_ellipse_2_traits_2.h>
#include <CGAL/convex_hull_2.h>
#include <CGAL/point_generators_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/min_quadrilateral_2.h>
#include <CGAL/rectangular_p_center_2.h>
#include <CGAL/bounding_box.h>
#include <cgal/Plane_3.h>
#include <CGAL/ch_melkman.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <set>
#include <gdiam.hpp>
// CH_3 for Minimum Oriented Bounding Box
#include <CGAL/convex_hull_3_to_polyhedron_3.h>
#include <CGAL/algorithm.h>
#include <CGAL/Delaunay_triangulation_3.h>

#ifndef H_TOOLKIT
#define H_TOOLKIT

class Toolkit{
	// definitions of kernel
	typedef CGAL::Exact_predicates_inexact_constructions_kernel  Kernel;
	typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

	typedef Kernel::Point_3 Point3;
	typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
	typedef Polyhedron::Halfedge_handle halfedge_handle;
	typedef Polyhedron::Halfedge_iterator halfedge_iterator;
	typedef Polyhedron::Facet_handle facet_handle;
	typedef Polyhedron::Facet_iterator facet_iterator;
	typedef Polyhedron::Vertex_handle vertex_handle;
	typedef Polyhedron::Vertex_iterator vertex_iterator;
	// AABB Tree elements
	typedef Kernel::Point_3 Point;
	typedef Kernel::Plane_3 Plane;
	typedef Kernel::Vector_3 Vector;
	typedef Kernel::Segment_3 Segment;
	typedef Kernel::Ray_3 Ray;
	typedef CGAL::AABB_face_graph_triangle_primitive<Polyhedron> Primitive;
	typedef CGAL::AABB_traits<Kernel, Primitive> Traits;
	typedef CGAL::AABB_tree<Traits> Tree;
	typedef boost::optional< Tree::Intersection_and_primitive_id<Segment>::Type > Segment_intersection;
	typedef boost::optional< Tree::Intersection_and_primitive_id<Ray>::Type > Ray_intersection;
	typedef boost::optional< Tree::Intersection_and_primitive_id<Plane>::Type > Plane_intersection;
	typedef Tree::Primitive_id Primitive_id;
	// Oriented-bounding box elements
	typedef K::Point_2 Point_2;
	typedef K::Vector_2 Vector_2;
	typedef K::Iso_rectangle_2 Iso_rectangle_2;
	typedef CGAL::Polygon_2<K> Polygon_2;
	typedef Polyhedron::Facet::Halfedge_around_facet_const_circulator HF_circulator;
	typedef Kernel::FT FT;
	// Another
	typedef Polyhedron::Vertex Vertex;
	typedef Vertex::Halfedge_around_vertex_const_circulator HV_circulator;
	typedef Vertex::Facet Facet;

	// Delaunay triangulation elements
	typedef K::Segment_2 Segment_2;
	typedef K::Iso_rectangle_2 Iso_rectangle_2;
	typedef CGAL::Triangulation_vertex_base_2<K>  Vertex_base;
	typedef CGAL::Constrained_triangulation_face_base_2<K> Face_base;



	template <class Gt, class Fb >
	class Enriched_face_base_2 : public Fb {
	public:
		typedef Gt Geom_traits;
		typedef typename Fb::Vertex_handle Vertex_handle;
		typedef typename Fb::Face_handle Face_handle;

		template < typename TDS2 >
		struct Rebind_TDS {
			typedef typename Fb::template Rebind_TDS<TDS2>::Other Fb2;
			typedef Enriched_face_base_2<Gt,Fb2> Other;
		};

	protected:
		int status;
	private:
		int mask ;

	public:
		Enriched_face_base_2(): Fb(), status(-1) {};

		Enriched_face_base_2(Vertex_handle v0, 
			Vertex_handle v1, 
			Vertex_handle v2)
			: Fb(v0,v1,v2), status(-1) {};

		Enriched_face_base_2(Vertex_handle v0, 
			Vertex_handle v1, 
			Vertex_handle v2,
			Face_handle n0, 
			Face_handle n1, 
			Face_handle n2)
			: Fb(v0,v1,v2,n0,n1,n2), status(-1) {};

		inline
			bool is_in_domain() const { return (status%2 == 1); };

		inline
			void set_in_domain(const bool b) { status = (b ? 1 : 0); };

		inline
			void set_mask(const int n) { mask = n ;} ;

		inline
			int get_mask() { return mask ;} ;

		inline 
			void set_counter(int i) { status = i; };

		inline 
			int counter() const { return status; };

		inline 
			int& counter() { return status; };
	}; // end class Enriched_face_base_2
	typedef Enriched_face_base_2<K, Face_base> Fb;
	typedef CGAL::Triangulation_data_structure_2<Vertex_base, Fb>  TDS;
	typedef CGAL::Exact_predicates_tag              Itag;
	typedef CGAL::Constrained_Delaunay_triangulation_2<K, TDS, Itag> CDT;
	typedef CGAL::Delaunay_mesh_size_criteria_2<CDT> Criteria;
	typedef CGAL::Lipschitz_sizing_field_2<K> Lipschitz_sizing_field;
	typedef CGAL::Lipschitz_sizing_field_criteria_2<CDT, Lipschitz_sizing_field> Lipschitz_criteria;
	typedef CGAL::Delaunay_mesher_2<CDT, Lipschitz_criteria> Lipschitz_mesher;
	typedef CDT::Vertex_handle Vertex_handle;
	typedef CDT::Face_handle Face_handle;
	typedef CDT::All_faces_iterator All_faces_iterator;
private:
	Eigen::MatrixXi F;
	Eigen::MatrixXd N;
	Eigen::MatrixXd temp;
	Eigen::MatrixXd V;
	std::map<vertex_handle,double> AO_MAP;
	Tree tree;
	Plane Symm;
	std::ofstream edgefile;
	std::vector<Point> S;
	CDT cdt;
	double diagonal;
	bool is_sym;
	unsigned int num_points;
	gdiam_real  * points;
	gdiam_point  * pnt_arr;
public:
	Eigen::VectorXd AO;
	struct TRI{
		Point source;
		Point target;
	};
	Polyhedron mesh;
	
	Toolkit(std::string Filename)
	{
		igl::readOFF(Filename, V, F);
		igl::per_vertex_normals(V,F,N);
#ifndef _DEBUG
		igl::ambient_occlusion(V,F,V,N,500,AO);
#endif
		AO = 1.0 - AO.array();
		std::fstream in = std::fstream(Filename);
		in >> mesh;
		preprocessmesh(mesh);
		in.close();
		tree.clear();
		tree.rebuild(faces(mesh).first, faces(mesh).second, mesh);
		tree.accelerate_distance_queries();
		std::cout << "AABB Tree construct done"<<std::endl;
#ifndef _DEBUG
		int i = 0 ;
		for(Polyhedron::Vertex_iterator it = mesh.vertices_begin(); it != mesh.vertices_end();
			it++ )
		{
			AO_MAP.insert(std::map<vertex_handle,double>::value_type(it,AO(i)));i++;
		}
#endif
		CalDiagonal();
		SetSymmetric(false);
		printf("Diagonal = %.4f\n",GetDiagonal());
		num_points =static_cast<unsigned int>(1.3 * mesh.size_of_vertices());
		points = (gdiam_point)malloc( sizeof( gdiam_point_t ) * num_points );
		pnt_arr = (gdiam_point *)malloc( sizeof( gdiam_point ) * num_points );
	}
	~Toolkit(){}
	double Show_AO();
	void insert(Eigen::MatrixXd V,Eigen::MatrixXd F);
	double AO_Integrate(std::vector<Point> list);
	Point refl(const Point input,const Plane & pl);
	int Find_Reflect();
	double Volume_OBB(Polyhedron & poly);
	unsigned int V_OBB(Polyhedron & ply , const double printv);
	void Clear_File(std::string filename);
	void Dimension(Point s,Point t,Plane pl,Point pop);
	void Dimension(Point_2 s,Point_2 t);
	//void Dimension(Point)
	void Close_File();
	void Open_File(std::string filename);
	void Search_S(Plane pl);
	bool Search_SFra(Plane pl);
	bool Initial_Printer(Polyhedron& poly);
	void Init_Num(int n);
	void Load_Edgefile(std::string filename);
	bool FillHole(Polyhedron & mesh);
	void CalDiagonal();
	double GetDiagonal();
	void CopyPolyhedron(Polyhedron start , Polyhedron & end);
	Kernel::Vector_3 compute_vertex_normal(Polyhedron::Vertex_iterator v);
	Kernel::Vector_3 compute_facet_normal(const Facet& f);
	double ConnectorsFeasible( );
	bool IsSymmetric() { return is_sym ; } ;
	Plane GetSymm() { return Symm; } ;
	void SetSymmetric(const bool b)  { is_sym = b ; } ;
private:
	void discoverComponent(const CDT & ct,Face_handle start,int index,std::list<CDT::Edge>& border );
	void discoverComponents(const CDT & ct);
	void initializeID(const CDT& ct);
public:
	void preprocessmesh(Polyhedron & p)
	{
		double shift[3] = { 0 };
		const unsigned int nb_p = p.size_of_vertices();
		for(Polyhedron::Point_const_iterator it = p.points_begin();
			it != p.points_end(); it++)
		{
			shift[0] += it->x() / nb_p;
			shift[1] += it->y() / nb_p;
			shift[2] += it->z() / nb_p;
		}
		for(Polyhedron::Point_iterator it = p.points_begin();
			it != p.points_end(); it++)
		{
			*it = Point(it->x()-shift[0],it->y()-shift[1],it->z()-shift[2]);
		}
	}
};
#endif